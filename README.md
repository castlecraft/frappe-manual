
## Frappe Manual

Frappe Framework Manual for Enterprise Teams.

[Documentation](https://castlecraft.gitlab.io/frappe-manual)

## Contribute

Clone and switch to directory

```shell
git clone https://gitlab.com/castlecraft/frappe-manual.git
cd frappe-manual
```

Re-Open in devcontainer

```shell
cp -R devcontainer-example .devcontainer
```

Setup and start frappe-manual

```shell
python -m venv env
. ./env/bin/activate
pip install -r requirements.txt
mkdocs serve
```

Open http://127.0.0.1:8000/frappe-manual in browser.
