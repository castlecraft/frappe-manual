## Introduction

Application needs to handle the API requests and background jobs with a strategy that doesn't deadlock the database. Application cannot scale beyond infrastructure limitations.

Infrastructure needs to scaled in case of sudden requirement of performance which will be back to normal after the surge. Infrastructure scaling cannot overcome application inefficiencies.

## Lean Development

To test viability of the application and infrastructure scaling:

- Decide a "happy path", Minimum viable user flows that are needed.
- Build a no frills and features API to test the flows decided before.
- Use any load testing tool to test the minimum viable API. ([locust.io](https://locust.io), [JMeter](https://jmeter.apache.org))
- Make changes to the API to meet the performance need. Add event logs, message brokers, job queues to handle the processing.

Once minimum viable API is ready for 80-90% of the cases. Start the actual application development with all features in depth.
