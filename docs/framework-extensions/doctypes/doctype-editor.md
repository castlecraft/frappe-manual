## DocType Editor

Doctype editor can be used to create/edit doctype.

Doctype editor is implemented to enforce maker/checker functionality, validations and sanity checks over creation and modification of doctypes.

### Steps for creating new doctype.

**1. For creating new doctype, user must check on create new checkbox.**

**2. Rest of the process will remain same as usual**

![Creating New DocType](/assets/doctype_editor_new_doctype.png)

### Editing existing doctype.

1. **From Doctype**: Choose the DocType which you want to edit in From Doctype field.

2. Once the doctype get selected, All other fields get populated into form and then can be edited as usual.

> Note: Doctype must be edited after form is loaded and saved. This ensures proper history changes being made to doctype.

![Editing Existing DocType](/assets/doctype_editor_edit_doctype.png)

> Note: For now validations are optional and can be bypassed in the future releases these will be enforced, Hence it is recommended to update any conflicting changes on existing doctypes.

### Workflow Control

We have added base workflow called `DocType Commitment Process` which could be modified for respective processes.

### Compare changes

DocType change comparison has been simplified with `compare changes`.

Delta for any edit operations can be seen with compare changes.

-  **DocType Editor**: Select the Doctype for which you wish to delta.

-  **Doctype Fields**: Display fields which are modified excluding child table updates.

-  **Doc Fields Edited**: Display fields which are modified in the child table.

-  **Doc Fields Created**: Doc fields that are newly created.

### 'is_docfield_master' checkbox

**To generate a Master Definition directly referencing a row of doctype, the following checkbox can be used.**

**A Docfield Master will be created with the exact definition from the respective row.**

![Creating the field Docfield Master](/assets/is_docfield_master_checkbox.png)
