## User Access Control

The **Access Control Doctype** is used for managing user permission, roles, and user permission rule within the framework. It provides a streamlined way to assign and control access for users by grouping `User Permission` and `User Permission Rule` under a unified framework.

### Steps for creating new doctype.

1. **User**: Select the user to whom the changes should apply. The permissions or rules configured in the Access Control table will be specific to the selected user.

2. **Module**: Select the module to filter the list of available doctypes. If no module is selected, the full list of doctypes will be displayed without filtering.

3. **Access Control**: Select the appropriate doctype for which you want to make changes. Based on the selected document, the changes will apply to the selected user's `User Permission` and `User Permission Rule`.
![Creating the User Access Control Doctype](/assets/user_access_control.png)
