## Access Control

The **Access Control** doctype provides a mechanism to manage roles, permissions rule, and user permissions within the system.

### Steps for creating new doctype.

1. **For Doctype**: Select the relevant doctype for which you are configuring access control.

2. **Enable User Permission**: A checkbox, if checked, makes the `Has Permission Rule` and `User Permission` table accessible, allowing further configurations.
![Creating the Access Control Doctype](/assets/access_control.png)

3. **Roles**: Assign roles to the user. You can select and assign as many roles as necessary.
![Roles](/assets/roles.png)

4. **Has Permission Rule**: This table allows users to define permissions by selecting specific *Permission Rule* for the selected doctype. This enables more granular control over which values within the doctype the user can access.
![Permission Rule](/assets/permission_rule.png)
>    For more details, you can refer to the [Permission Rule](https://frappe-manual-castlecraft-b249c70d8b6d99bd095c0ef03e4a3115a94f5.gitlab.io/framework-extensions/doctypes/permission-rule/) documentation.

5. **User Permission**:
  This table allows users to define specific permissions for a particular doctype and its documents. Each row in the table has the following columns:

    - **Allow**: Represents the doctype for which the user permission is being set.

    - **For Value**: Refers to a specific document within the selected doctype.

    - **Apply to All Doctypes**: A checkbox field that determines whether this permission applies to all doctypes.
![User Permission](/assets/user_permission.png)
