
# OAuth2 OIDC

Use [castlecraft](https://github.com/castlecraft/cfe) app to override auth. It allows you to accept any OAuth 2/OpenID `access_token` or `id_token` in you frappe app.

The `userinfo` endpoint or the `id_token` must have a claim with email address of the user. That user will be set for incoming request with appropriate `Authorization` header.

## Configuration

To enable adding errors `Error Log`(s) set `castlecraft_enable_log` in `site_config.json` to `1`.

Following configuration works from version 2 of the castlecraft app.

## Add Identity Provider

User with role `System Manager` add, update or delete `CFE Identity Provider` DocType.

Identity Provider is of 2 types. It can Introspect token and call profile endpoint. Or it can issue id_token as signed jwt which can be verified using jwks endpoint.

Shortcut to Identity Provider management is available under `Castlecraft Auth` workspace. It is visible only to `System Manager`(s).

### Common Properties

![image](/assets/cfe-oidc/cfe-idp-common-config.png)

- **Enabled**: Only one provider can be enabled at a time.
- **Authorization Type**: Select from `Introspection` or `JWT Verification`.
- **First Name Key**: Property of userinfo endpoint response or id token claim which holds first name.
- **Email Key**: Property of userinfo endpoint response or id token claim which holds email.
- **Full Name Key**: Property of userinfo endpoint response or id token claim which holds full name.
- **Create User**: If enabled user will be created if not found in database.
- **User Roles**: User roles assigned to user when the user is created.

![image](/assets/cfe-oidc/cfe-idp-user-config.png)

- **User Field Map**: Properties of userinfo endpoint or id token claims that need to be stored attached to user. It will be available as claims in `/api/method/frappe.integrations.oauth2.openid_profile` response.

### Introspection Config

![image](/assets/cfe-oidc/cfe-idp-introspection-config.png)

- **Auth Header Enabled**: Enable if introspection endpoint needs basic auth using client id and client secret.
- **Fetch User Info**: Enable if additional claims for the token are available on userinfo endpoint.
- **Client ID**: Client ID used for authentication on introspection endpoint.
- **Client Secret**: Client Secret used for authentication on introspection endpoint.
- **Token Key**: POST request property used to send the token to introspection endpoint. E.g. `token`
- **Introspection Endpoint**: URL of introspection endpoint.
- **Profile Endpoint**: URL of userinfo endpoint.

### JWT Verification Config

![image](/assets/cfe-oidc/cfe-idp-jwt-config.png)

- **JWKS Endpoint**: URL of jwks endpoint.
- **Allowed Audience**: List of allowed `aud` in jwt.

## Backchannel Logout

Following endpoint will verify the jwt for backchannel logout.

Backchannel logout endpoint: `/api/method/castlecraft.services.oauth2.back_channel_logout`. Example: `https://example.com/api/method/castlecraft.services.oauth2.back_channel_logout`

## Sync Claims from IDP

Following endpoint will sync user claims as per IDP configuration.

Sync claims endpoint: `/api/method/castlecraft.services.oauth2.sync_claims`. Example: `https://example.com/api/method/castlecraft.services.oauth2.sync_claims`
