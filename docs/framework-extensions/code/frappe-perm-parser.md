## frappe_perm_parser

For cases where a custom query is created and permissions need to be validated use `has_valid_perms` function to validate doctype access.

```py
    has_valid_perms(
        qb,                             # Query builder or pure SQL query
        exclude=[],                     # Doctype names that should be bypass validation
        user=None,                      # Specify user or session user will be used
        ptype="read",                   # Permission type (Default = "read")
        raise_exception=False           # Throws frappe.permission error with standard message if true
        append_query_validations=True   # Appends query validation for field level and global permissions
    )
```

### Examples

- Frappe Query Builder

```py
import frappe
from frappe_utils.frappe_perm_parser.frappe_perm_parser import has_valid_perms

WebPageView = frappe.qb.DocType("Note")

builder = frappe.qb.from_(WebPageView).select("*")

builder = has_valid_perms(builder) # Throws frappe permission error or returns query with appended filters.

return builder.run()
```

- Frappe DB SQL

```py
import frappe
from frappe_utils.frappe_perm_parser.frappe_perm_parser import has_valid_perms

query = f"""
    SELECT `path`,
            COUNT(*) as count,
            COUNT(CASE WHEN CAST(`is_unique` as Integer) = 1 THEN 1 END) as unique_count
    FROM `tabWeb Page View`
    WHERE `creation` BETWEEN {some_date} AND {some_later_date}
    """

query = has_valid_perms(query) # Throws frappe permission error or returns true.

return frappe.db.sql(query)
```
