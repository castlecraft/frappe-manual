# Introduction

Frappe MDM is build with focus on solving multi-site dependency and providing frappe with ability create better more **distributed** and vertically and horizontally **scalable solutions**.

Frappe MDM has implementation extending frappe virtual doctypes and have ability where you can create virtual doctypes and declare them as `MDM Masters` this will publish them to a configured DB where both the doctype data and meta will be maintained.

#### Supported DB Adapters

-  Mongo DB

## How to use?

After you have setup the pre-requisites and have DB Adapter configured you can follow these steps that will cover

-  Creation and declaration of master
-  Access control over sharing resources to other sites
-  How other sites can consume shared doctype
-  How shared data can be verified realtime

### Creation and declaration of master

Master declarations can be done directly from doctype level for any `virtual` doctype

#### Example

Creating a doctype and declaring it as `Master` to consume from MongoDB


![type:video](/assets/frappe-mdm/declaring_master.mp4)


After above step doctype is now configured with mongodb and you can start using it

![type:video](/assets/frappe-mdm/adding_records.mp4)


### Access control over sharing resources to other sites

To share the above created doctype to other sites in infra can be done using `Frappe Shared Resource`

#### Fields

-  **Group Name**: Group name.
-  **For Site**: Specify the site you would like to share.
-  **Shared Doctype**: You can specify the doctype in this table you would like to share.

![type:video](/assets/frappe-mdm/create_frappe_shared_resource_group.mp4)



With this step `site4.localhost` can now consume `Item Master` on their end.

### How other sites can consume shared doctype

For `site4.localhost` to consume the shared doctype they can go to `Frappe Shared Resource` and consume any doctype shared with them.

![type:video](/assets/frappe-mdm/consume_doctype.mp4)


With above step `Item Master` is now available on `site4.localhost` for consumption as local doctype with shared data from owner.

### How shared data can be verified realtime

To verify that `Item Master` is synced and connected

![type:video](/assets/frappe-mdm/realtime_udpate.mp4)
