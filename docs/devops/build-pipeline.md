## Build Pipeline

Following commands will use python script [`generate_apps_json.py`](https://github.com/castlecraft/custom_containers/blob/main/frappe-builder-repo/generate_apps_json.py) to render `apps.json` from directory mentioned in `$APPS_JSONS` environment variable or defaults to directory called `benches`.

Sample `apps.json` Jinja2 template found here [`frappe-builder-repo/apps.json`](https://github.com/castlecraft/custom_containers/blob/main/frappe-builder-repo/benches/apps.json)

Copy all files from [`frappe-builder-repo`](https://github.com/castlecraft/custom_containers/blob/main/frappe-builder-repo) to root of your "builder-repo"

Execute following to build image from root of your builder repo

```shell
python -m venv env
. ./env/bin/activate
pip install -U pip Jinja2
source ./common.env
export REPO_PAT=revant
export APPS_JSON_BASE64=$(python generate_apps_json.py -t apps.json | base64 -w 0)
docker build \
  --build-arg=FRAPPE_PATH=https://github.com/frappe/frappe \
  --build-arg=FRAPPE_BRANCH=version-14 \
  --build-arg=PYTHON_VERSION=3.11.4 \
  --build-arg=NODE_VERSION=16.20.1 \
  --build-arg=APPS_JSON_BASE64=$APPS_JSON_BASE64 \
  --tag=ghcr.io/org/repo/image:1.0.0 \
  --file=Containerfile .
```

You can customize the [Containerfile](https://github.com/castlecraft/custom_containers/blob/main/frappe-builder-repo/Containerfile). You can copy additional files by placing the under `resources` directory and specifying `COPY` in `Containerfile`.
