# Deployments

- Use docker swarm based setup in case of single VM setup that will gradually scale to a cluster.
- Use Kubernetes if setup is focused on running on a cluster from first day.
- Setup environment for developers to mock production, for user to test development, for staging of production and actual production.
- DO NOT automate production deployments. Add approvals and use GitOps in production.
