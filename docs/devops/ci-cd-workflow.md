![CI-CD-Workflow](/assets/ci-cd-workflow.png)

- Manage the app code in app and app fork repos.
- App code is verified and tested on merge requests to app repos.
- Keep the build recipes, Containerfile and related resources in builder-repo.
- Trigger to builder repo build will build image from give `apps.json` combination.
- Trigger deploy on environment once image is ready in previous step.
