### Huge file with lot of lines of code

At times, developers get carried away with delivery, resulting in jargon lines of code that should be broken down. We should follow a standard where '.py' files should not exceed a **finite number** of lines (preferably 1000, anything beyond that needs to be considered).

**Example**

Break down functions and code into structure with better entrypoint and tree.

`Naive`

```python
.
├── purchase_request.py
    ``
    def process_pr():
        <LOGIC_ON_MASTER_1>...
        <LOGIC_ON_MASTER_2>...
        <LOGIC_ON_MASTER_3>...
        <LOGIC_ON_MASTER_4>...
```

`Recommended`

```python
.
├── purchase_request.py(entry_point)
│     │     def process_pr():
│     │         <master_1_file.LOGIC_ON_MASTER_1>...
│     │         <master_2_file.LOGIC_ON_MASTER_2>...
│     │         <master_3_file.LOGIC_ON_MASTER_3>...
│     │         <master_4_file.LOGIC_ON_MASTER_4>...
│     │
│     │
├── purchase_request
│     ├── purchase_request_master_1.py
│     │     def LOGIC_ON_MASTER_1():
|     |         <master_1_file.LOGIC_ON_MASTER_1>...
│     │
│     ├── purchase_request_master_1.py
│     │     def LOGIC_ON_MASTER_1():
|     |         <master_1_file.LOGIC_ON_MASTER_1>...
│     │
│     ├── purchase_request_master_1.py
│     │     def LOGIC_ON_MASTER_1():
|     |         <master_1_file.LOGIC_ON_MASTER_1>...
│     │
│     └── purchase_request_master_1.py
│           def LOGIC_ON_MASTER_1():
|               <master_1_file.LOGIC_ON_MASTER_1>...
│
└── ...
```
