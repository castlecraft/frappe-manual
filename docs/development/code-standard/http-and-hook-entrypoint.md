
    Every code entry-point, whether 'API' or 'Hooks', should have a clean init that is separate from any form of business logic, this will assist in the future when we require entry-point controls, such as 'decorators'.


### Entrypoint for hooks

Hooks having single entrypoint ensure no redundant hooks are inserted and accordingly **sequence** of hook calls can be controlled as per logic.

**Example**

`Current`

`purhcase_request.py = on_updated = <LOGIC>`

```python
class PurchaseRequest:
    def on_update(doc):
        do_this...
        ...
        do_that...
        ...
```

`Suggested`

`purchase_controller.py = on_update => purhcase_request.py = <LOGIC>`

```python
# purhcase_request.py
class PurchaseRequest:
    def on_update(doc):
        fun_do_this()
        fun_do_that()

#this.py
def fun_do_this():
    do_this.....

#that.py
def fun_do_that():
    do_that.....
```

### Entrypoints for API

APIs should also follow same entry-point standards.

**Example**

`Naive`

`API => purhcase_request.py = <LOGIC>`

```python
@frappe.whitelist()
def do_something():
    <CODE_LOGIC_STARTED>..
```

`Recommended`

`API => purchase_controller.py => purhcase_request.py = <LOGIC>`

```python
# purchase_controller.py
@frappe.whitelist()
def do_something():
    purhcase_request.code_logic()

#purchase_request.py
def code_logic():
    <CODE_LOGIC_STARTED>..

```
