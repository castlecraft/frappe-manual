
## Domain specific structure

We should break down code and file structure based on domain specific logic and reuse such components which may be commonly imported at other, example any type of master operation should be common such that it can be reused at other places where custom implementation are done.

**Example**
```python
def purchase_request():
        <MASTER_1_OPERATION_A>...
        <LOGIC_ON_MASTER_2>...
        <MASTER_1_OPERATION_B>...

def purchase_request_2():
        <COPY_MASTER_1_OPERATION_A>...
        <SOMETHING_ELSE>
        <COPY_MASTER_1_OPERATION_B>...
```

```python
# Structure
.
├── purchase_request
│     ├── purchase_request.py
│     ├── purchase_request_items.py
│     └── purchase_request_operation.py
│
└── ...

# purchase_request.py
    def purchase_request():
        self.validate_purchase_request()
        purchase_request_items.validate_item_status()
        purchase_request_operation.code_logic()

    def purchase_request_2():
        purchase_request_items.validate_item_status()
        self.specific_2()
        purchase_request_operation.code_logic()

# purchase_request_items.py
    def validate_item_status():
        <CODE_LOGIC_STARTED>..

# purchase_request_operations.py
    def code_logic():
        <CODE_LOGIC_STARTED>..

```
