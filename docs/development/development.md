## Development

### Setup development environment

- Use VS Code devcontainer. It needs a `.devcontainer` directory in your VS Code project directory to start development using containers. Create a repo for your developers to bootstrap development environment using devcontainer. Add additional scripts to do chores.
- Basic devcontainer setup for frappe exists in [`devcontainer-example`](https://github.com/frappe/frappe_docker/tree/main/devcontainer-example) directory of frappe/frappe_docker
- Container can be customized with custom image and additional dependencies installed as part of initialization.
- Refer other frappe example located under [`kube-devcontainer`](https://github.com/castlecraft/custom_containers/tree/main/kube-devcontainer) to build custom image using `Dockerfile`, `devcontainer.json` and `compose.yml`


### YouTube Video related to development setup

<iframe width="560" height="315" src="https://www.youtube.com/embed/xOgdMcGW56U?si=H73jJbcCydtLHuwD" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
