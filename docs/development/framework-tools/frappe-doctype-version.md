## Frappe doctype version
Existing frappe doctype provides functionality to track doctype updates based on following criteria.

- Properties that changed
- Rows added
- Rows removed
- Properties in child tables that are changed.

`Frappe version doctype can be consumed from following APIs`

- **Doctype REST API**

  Frappe version comes with full power of Doctype REST API and respective could be used to fetch, filter and CRUD over them as per documentation https://frappeframework.com/docs/user/en/api/rest.

- **Frappe Method** (http://site2.localhost:8000?doctype=Note%20Temp&name=d2a5210a3b&_=1696333110320)
  Frappe Get Doc method `/api/method/frappe.desk.form.load.getdoc` can be used with filters as follows to get versions of respective doctype.

  It accepts following URL params `doctype={DOCTYPE_NAME}` `_={DOCTYPE_ID}`

  Response will be in following format
  ```
    docs: [DOC_WIT_MATCHING_ID],
    docinfo: {
        ...version meta_data,
        versions: [
            LIST_OF_VERSIONS
        ]
    }
  ```

[//]: <> (Add how to query changed fields on SQL.)
