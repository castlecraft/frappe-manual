## UOM Doctypes

A Unit of Measure (UOM) is a unit using which an Item is measured.

1. **UOM Category**
   ![UOM Category](../../../assets/uom/uom-category.png)
   A UOM (Unit of Measure) Category is a way to group similar units of measure.
   For example, you might have a UOM Category for length that includes units like meters, centimeters, and millimeters.
   Another UOM Category for weight might include units like kilograms and grams.
   This Doctype contains only one field Category Name.

2. **UOM**
   ![UOM](../../../assets/uom/uom.png)
   UOM (Unit of Measure) doctype contains 3 fields:
    - **Enabled:** If checked, the UOM is available to use else it is not available to use anywhere. However existing documents will continue to use it.
    - **Name:** Name of the UOM.
    - **Must be a Whole Number:** If checked, the value of the UOM must be a Whole number else it will throw an error. Eg: Checked in case of Dozen or Unit and Unchecked in case of Kg or Pound.

3. **UOM Conversion Factor**
   ![UOM Conversion Factor](../../../assets/uom/uom-conversion-factor.png)
   UOM (Unit of Measure) Conversion Factor is a crucial feature that allows you to define the relationship between different units of measure for the same item. This conversion factor is necessary when dealing with diverse units for the same product in various aspects of your business, such as purchasing, selling, and inventory management.
   Here's how UOM Conversion Factor works:

    - **Category:** This is the UOM Category i.e. Mass, Energy, Density etc.
    - **From:** This refers to the original or source unit of measure that you are converting from. It is the unit in which the quantity is initially specified.
    - **To:** This is the target or destination unit of measure to which you want to convert the quantity. It is the unit in which you want the quantity to be represented or stored.
    - **Value:** It represents the numerical factor by which you multiply the quantity in the `From UOM` to obtain the equivalent quantity in the `To UOM`. It defines the conversion ratio between the two units.

4. **UOM Conversion Detail**
   ![UOM Conversion Detail](../../../assets/uom/uom-conversion-detail.png)
   UOM Conversion Detail is a child table that appears while creating a New Item.
   This table contains two fields:
    - **UOM:** This defines the UOM that is to be associated with the Item.
     Eg: For an Item Rice, UOM's could be Tonnes, Kg, Milligrams etc.
    - **Conversion Factor:** Conversion Factor defines the multiplication factor between different uoms of same category. These values are automatically populated if the conversion for the same is available in the UOM Conversion Factor doctype. Default UOM is mandatory for the conversion.

## Implementation

UOM can be implemented in the following manner:

- Create doctype `UOM Category`.
![UOM Category Implementation](../../../assets/uom/implementation/uom-category.png)

- Create doctype `UOM`.
![UOM Category](../../../assets/uom/implementation/uom.png)

- Create doctype `UOM Conversion Factor`.
![UOM Conversion Factor](../../../assets/uom/implementation/uom-conversion-factor.png)

- Create doctype `UOM Conversion Detail` and set `Is Child Table`.
![UOM Conversion Detail](../../../assets/uom/implementation/uom-conversion-detail.png)

UOM Implementation is complete here.

Now we consume the UOM in Item and Item Price.

- Create doctype `Item`. Set `UOMs` to `UOM Conversion Detail` table.
![Item](../../../assets/uom/implementation/item.png)
  Set the `Display Depends On` property for the `UOMs` table so that it is only visible when the `Default UOM` is selected.
![Display Depends On](../../../assets/uom/implementation/item-display-depends.png)

- Include functions in [item.js](https://gitlab.com/castlecraft/dev_utils/-/blob/main/dev_utils/dev_utils/doctype/item/item.js?ref_type=heads) to set UOMs table to blank when the default UOM is changed and also to call a whitelisted function from [item.py](https://gitlab.com/castlecraft/dev_utils/-/blob/main/dev_utils/dev_utils/doctype/item/item.py?ref_type=heads) to fetch the relevant conversion factor for a particular UOM in the table.

- Create doctype `Item Price`.
![Item Price](../../../assets/uom/implementation/item-price.png)

Using Item and Item Price we can create Sales Invoice Item and Sales Invoice.

- Create doctype `Sales Invoice Item` and set `Is Child Table`.
![Sales Invoice Item](../../../assets/uom/implementation/sales-invoice-item.png)

- Create doctype `Sales Invoice`.
![Sales Invoice](../../../assets/uom/implementation/sales-invoice.png)

- Include functions in [sales-invoice.js](https://gitlab.com/castlecraft/dev_utils/-/blob/main/dev_utils/dev_utils/doctype/sales_invoice/sales_invoice.js?ref_type=heads) to accomplish the following tasks:
    1. Fetch the uom, rate, quantity for the uom and calculate the amount when a particular item is selected.
    2. Get the conversion factor when the UOM is changed.
    3. Calculate the amount whenever uom, qty and rate gets manipulated.

- Add function in [sales-invoice.py](https://gitlab.com/castlecraft/dev_utils/-/blob/main/dev_utils/dev_utils/doctype/sales_invoice/sales_invoice.py?ref_type=heads) to throw an error when a UOM is set to whole number and a fraction is entered or vice versa.

## Example

**1. Create UOM Category**
![UOM Category](../../../assets/uom/uom-category.png)

Create a UOM Category `Mass`.

**2. Create UOM**
![UOM](../../../assets/uom/uom.png)

Create a UOM `Grams` and since grams are not whole numbers we keep the `Must be a Whole Number` unchecked. Similarly multiple UOMs like Tonne, Kg, Milligrams etc can be created.

**3. Create UOM Conversion Factor**
![UOM Conversion Factor](../../../assets/uom/uom-conversion-factor.png)

- Select a UOM Category.
- Select `From` and `To` UOM.
- Enter the multiplication factor for conversion.
- Save.

**4. Create Item**
![Item](../../../assets/uom/item.png)

- Enter the `Item Name` and `Item Code`.
- Select the Default UOM.
- Add the relevant UOMs to the table depending upon the item. The corresponding `Conversion Factor` will be automatically populated.
- Save.

**5. Create Item Price**
![Item Price](../../../assets/uom/item-price.png)

- Select Item. `Name` and `UOM` be automatically populated.
- Enter the `Rate` for the item for the particular UOM.
- Save.

**6. Create Sales Invoice**
![Sales Invoice](../../../assets/uom/sales-invoice.png)

- Add `Item` to the table. All the other details are automatically populated.
- `Quantity` and `Rate` changes will affect the `Amount`
