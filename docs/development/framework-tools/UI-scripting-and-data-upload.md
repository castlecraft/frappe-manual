For cases where we need to populate data on frappe doctype UI below example can be used

**Use Case** Let's consider we are creating doctypes and for populating doctype `fields` from data sources such as JSON, excel, csv, etc.

### Navigate to creating a new doctype and Open Chrome console

Windows: `Ctrl + Shift + J`. Mac: `Cmd + Opt +J`.

![image](/assets/UI-scripting-and-data-upload-01.png)

### Paste the following code in your browser console

```
let data = []

function appendData(docField="fields"){
    data = data || [];

    for (let i = 0; i < data.length; i++) {
        const doc = data[i];
        doc.parentfield = docField;

        cur_frm.add_child(docField,doc);
        cur_frm.refresh_fields();
    }
}
```

### Assign any data to the `data` variable as array items and call the function

```JS
data = [
    {
        "label": "Name",
        "docstatus": 0,
        "name": "new-docfield-1",
        "fieldtype": "Data",
        "reqd": 0,
        "read_only": 0,
        "parenttype": "DocType",
    },
    {
        "label": "Title",
        "docstatus": 0,
        "name": "new-docfield-2",
        "fieldtype": "Data",
        "reqd": 0,
        "read_only": 0,
        "parenttype": "DocType",
    },
];

appendData()
```

**Paste the above into your console and it should populate data.**

![image](/assets/UI-scripting-and-data-upload-02.png)

<details>
<summary>Example Docfield Data</summary>

```JSON
{
    "docstatus": 0,
    "doctype": "DocField",
    "name": "new-docfield-1",
    "__islocal": 1,
    "__unsaved": 1,
    "owner": "Administrator",
    "fieldtype": "Data",
    "precision": "",
    "non_negative": 0,
    "hide_days": 0,
    "hide_seconds": 0,
    "reqd": 0,
    "is_virtual": 0,
    "search_index": 0,
    "sort_options": 0,
    "show_dashboard": 0,
    "fetch_if_empty": 0,
    "hidden": 0,
    "bold": 0,
    "allow_in_quick_entry": 0,
    "translatable": 0,
    "print_hide": 0,
    "print_hide_if_no_value": 0,
    "report_hide": 0,
    "collapsible": 0,
    "hide_border": 0,
    "in_list_view": 0,
    "in_standard_filter": 0,
    "in_preview": 0,
    "in_filter": 0,
    "in_global_search": 0,
    "read_only": 0,
    "allow_on_submit": 0,
    "ignore_user_permissions": 0,
    "allow_bulk_edit": 0,
    "permlevel": 0,
    "ignore_xss_filter": 0,
    "unique": 0,
    "no_copy": 0,
    "set_only_once": 0,
    "remember_last_selected_value": 0,
    "parent": "new-doctype-1",
    "parentfield": "fields",
    "parenttype": "DocType",
    "idx": 1,
    "__unedited": false,
    "label": "test"
}
```

</details>
